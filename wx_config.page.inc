<?php

/**
 * @file
 * Contains wx_config.page.inc..
 *
 * Page callback for Wx config entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Wx config templates.
 *
 * Default template: wx_config.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_wx_config(array &$variables) {
  // Fetch wxConfig Entity Object.
  $wx_config = $variables['elements']['#wx_config'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
