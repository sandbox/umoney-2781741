<?php

/**
 * @file
 * Contains wx_message.page.inc.
 *
 * Page callback for Wx message entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Wx message templates.
 *
 * Default template: wx_message.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_wx_message(array &$variables) {
  // Fetch wxMessage Entity Object.
  $wx_message = $variables['elements']['#wx_message'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
