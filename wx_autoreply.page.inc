<?php

/**
 * @file
 * Contains mx_autoreply.page.inc..
 *
 * Page callback for Mx autoreply entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Mx autoreply templates.
 *
 * Default template: mx_autoreply.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_mx_autoreply(array &$variables) {
  // Fetch MxAutoreply Entity Object.
  $mx_autoreply = $variables['elements']['#mx_autoreply'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
