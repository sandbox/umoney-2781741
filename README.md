INTRODUCTION
------------
 One Weixin support wechat auth, message process, menu handle, and 
 support multiple wechat in one site,it is useful for wechat official accounts operator.

INSTALLATION
------------
No special install steps are necessary to use this module, see https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.

NOTES
-----
This module response to wechat request. Therefore, you need
to own one/more wechat official accounts for this module to work.
About how to apply wechat Official Accounts, please visit wechat development site https://mp.weixin.qq.com
and wechat enterprise development site https://qy.weixin.qq.com


MAINTAINERS
-----------
Current maintainers:

 * Umoney (https://www.drupal.org/u/umoney)
