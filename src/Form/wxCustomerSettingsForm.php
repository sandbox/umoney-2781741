<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Form\wxCustomerSettingsForm.
 */

namespace Drupal\one_weixin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class wxCustomerSettingsForm.
 *
 * @package Drupal\one_weixin\Form
 *
 * @ingroup weixin
 */
class wxCustomerSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'wxCustomer_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Defines the settings form for wx customer entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['wxCustomer_settings']['#markup'] = 'Settings form for wx customer entities. Manage field settings here.';
    return $form;
  }

}
