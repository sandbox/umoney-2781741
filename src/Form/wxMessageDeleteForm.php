<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Form\wxMessageDeleteForm.
 */

namespace Drupal\one_weixin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting wx message entities.
 *
 * @ingroup weixin
 */
class wxMessageDeleteForm extends ContentEntityDeleteForm {

}
