<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Form\wxAutoreplyDeleteForm.
 */

namespace Drupal\one_weixin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting wx autoreply entities.
 *
 * @ingroup weixin
 */
class wxAutoreplyDeleteForm extends ContentEntityDeleteForm {

}
