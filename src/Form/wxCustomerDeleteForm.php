<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Form\wxCustomerDeleteForm.
 */

namespace Drupal\one_weixin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting wx customer entities.
 *
 * @ingroup weixin
 */
class wxCustomerDeleteForm extends ContentEntityDeleteForm {

}
