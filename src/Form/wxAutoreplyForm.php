<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Form\wxAutoreplyForm.
 */

namespace Drupal\one_weixin\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for wx autoreply edit forms.
 *
 * @ingroup weixin
 */
class wxAutoreplyForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\one_weixin\Entity\wxAutoreply */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label wx autoreply.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label wx autoreply.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.wx_autoreply.canonical', ['wx_autoreply' => $entity->id()]);
  }

}
