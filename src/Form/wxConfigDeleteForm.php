<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Form\wxConfigDeleteForm.
 */

namespace Drupal\one_weixin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting wx config entities.
 *
 * @ingroup weixin
 */
class wxConfigDeleteForm extends ContentEntityDeleteForm {

}
