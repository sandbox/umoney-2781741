<?php

/**
 * @file
 * Contains \Drupal\weixin\wxAutoreplyAccessControlHandler.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the wx autoreply entity.
 *
 * @see \Drupal\weixin\Entity\wxAutoreply.
 */
class wxAutoreplyAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\weixin\wxAutoreplyInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished mx autoreply entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published mx autoreply entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit mx autoreply entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete mx autoreply entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add mx autoreply entities');
  }

}
