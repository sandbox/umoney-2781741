<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxCustomerInterface.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining wx customer entities.
 *
 * @ingroup weixin
 */
interface wxCustomerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the wx customer name.
   *
   * @return string
   *   Name of the wx customer.
   */
  public function getName();

  /**
   * Sets the wx customer name.
   *
   * @param string $name
   *   The wx customer name.
   *
   * @return \Drupal\one_weixin\wxCustomerInterface
   *   The called wx customer entity.
   */
  public function setName($name);

  /**
   * Gets the wx customer creation timestamp.
   *
   * @return int
   *   Creation timestamp of the wx customer.
   */
  public function getCreatedTime();

  /**
   * Sets the wx customer creation timestamp.
   *
   * @param int $timestamp
   *   The wx customer creation timestamp.
   *
   * @return \Drupal\one_weixin\wxCustomerInterface
   *   The called wx customer entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the wx customer published status indicator.
   *
   * Unpublished wx customer are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the wx customer is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a wx customer.
   *
   * @param bool $published
   *   TRUE to set this wx customer to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\wxCustomerInterface
   *   The called wx customer entity.
   */
  public function setPublished($published);

}
