<?php

/**
 * @file
 * Contains \Drupal\weixin\Controller\wxController.
 */

namespace Drupal\one_weixin\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\one_weixin\impl\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\DependencyInjection\Container;
use Drupal\Core\Cache\Context\HeadersCacheContext;
use Drupal\Core\Cache\Context\CookiesCacheContext;
use Drupal\Core\Cache\Context\SessionCacheContext;
use Drupal\Core\Cache\Context\UserCacheContext;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;

use Drupal\one_weixin\Entity\wxMessage;
use Drupal\one_weixin\Entity\wxCustomer;
use Drupal\one_weixin\impl\wxCommon;
use Drupal\one_weixin\impl\DrupalWechat;
use Drupal\one_weixin\impl\DrupalQYWechat;
use Drupal\one_weixin\Entity\wxConfig;
use Drupal\one_weixin\impl\WechatXmlResponse;

/**
 * Class wxController.
 *
 * @package Drupal\weixin\Controller
 */
class WechatController extends ControllerBase {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request_stack;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var Drupal\Core\Config\ConfigManager
   */
  protected $config_manager;

  /**
   * Drupal\Core\DependencyInjection\Container definition.
   *
   * @var Drupal\Core\DependencyInjection\Container
   */
  protected $service_container;

  /**
   * Drupal\Core\Cache\Context\HeadersCacheContext definition.
   *
   * @var Drupal\Core\Cache\Context\HeadersCacheContext
   */
  protected $cache_context_headers;

  /**
   * Drupal\Core\Cache\Context\CookiesCacheContext definition.
   *
   * @var Drupal\Core\Cache\Context\CookiesCacheContext
   */
  protected $cache_context_cookies;

  /**
   * Drupal\Core\Cache\Context\SessionCacheContext definition.
   *
   * @var Drupal\Core\Cache\Context\SessionCacheContext
   */
  protected $cache_context_session;

  /**
   * Drupal\Core\Cache\Context\UserCacheContext definition.
   *
   *  @var Drupal\Core\Cache\Context\UserCacheContext
   */
  protected $cache_context_user;

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger_factory;
  protected $wid;
  protected $wechat;
  protected $qywechat;
  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, ConfigManager $config_manager, Container $service_container, HeadersCacheContext $cache_context_headers, CookiesCacheContext $cache_context_cookies, SessionCacheContext $cache_context_session, UserCacheContext $cache_context_user, LoggerChannelFactory $logger_factory) {
    $this->request_stack = $request_stack;
    $this->config_manager = $config_manager;
    $this->service_container = $service_container;
    $this->cache_context_headers = $cache_context_headers;
    $this->cache_context_cookies = $cache_context_cookies;
    $this->cache_context_session = $cache_context_session;
    $this->cache_context_user = $cache_context_user;
    $this->logger_factory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.manager'),
      $container->get('service_container'),
      $container->get('cache_context.headers'),
      $container->get('cache_context.cookies'),
      $container->get('cache_context.session'),
      $container->get('cache_context.user'),
      $container->get('logger.factory')
    );
  }

  public function wxcallback($wid) {
    $request = $this->request_stack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->logger_factory->get('weixin');
    $logger->notice('call back content @aa,@bb ',array('@aa'=>$wid,'@bb'=>var_export($content,TRUE)));
    $this->wid = $wid;

    if($method=='GET') {
      $this->wechat = new DrupalWechat($wid);
      $result = false;
      if (isset($params["echostr"])) {
        if($this->wechat->checkSignature($params)) {
          $result =  $params["echostr"];
        }
      }
      $response = new WechatXmlResponse($result?$result:'false');
      $response->addCacheableDependency($request);
      return $response;

    }elseif($method=='POST') {
      $xmlstr = '';
      $this->wechat = new DrupalWechat($wid,NULL,$content);
      $type = $this->wechat->getMsgType();
      switch(strtolower($type)) {
        case wxCommon::MSGTYPE_TEXT:
          $this->wechat->msgUpdate();
          break;
        case wxCommon::MSGTYPE_EVENT:
          $this->wechat->eventUpdate();
          break;
        case wxCommon::MSGTYPE_IMAGE:
          break;
        default:
          break;
      }
      $openid = $this->wechat->getFromUser();
      $this->wechat->customerUpdate($openid);
      $xmlstr = $this->callExtension();
      if(!$xmlstr){
        $xmlstr = $this->wechat->getReplyMsg('你好');
      }
      $response = new WechatXmlResponse($xmlstr);
      return $response;
    }
    $response = new WechatXmlResponse($content);
    return $response;
  }

  public function qycallback($wid) {
    $request = $this->request_stack->getCurrentRequest();
    $params = $request->query->all();
    $content = $request->getContent();
    $method = $request->getMethod();
    $logger = $this->logger_factory->get('qyweixin');
    $logger->notice('call back content @aa,@bb ',array('@aa'=>$wid,'@bb'=>var_export($content,TRUE)));
    $this->wid = $wid;

    if($method=='GET') {
      $this->wechat = new DrupalQYWechat($wid);
      $result = false;
      if (isset($params["echostr"])) {
        $encryptStr = $params["echostr"];
        if($this->wechat->checkSignature($params,$encryptStr)) {
          $result = $this->wechat->decrypt($encryptStr);
        }
      }
      $response = new WechatXmlResponse($result?$result:'false');
      $response->addCacheableDependency($request);
      return $response;

    }elseif($method=='POST') {
      $xmlstr = '';
      $this->wechat = new DrupalQYWechat($wid,NULL,$content);
      $type = $this->wechat->getMsgType();
      switch(strtolower($type)) {
        case wxCommon::MSGTYPE_TEXT:
          $this->wechat->msgUpdate();
          break;
        case wxCommon::MSGTYPE_EVENT:
          $this->wechat->eventUpdate();
          break;
        case wxCommon::MSGTYPE_IMAGE:
          break;
        default:
          break;
      }
      $openid = $this->wechat->getFromUser();
      $this->wechat->customerUpdate($openid);
      $xmlstr = $this->callExtension();
      if(!$xmlstr){
        $xmlstr = $this->wechat->getReplyMsg('你好');
      }
      $response = new WechatXmlResponse($xmlstr);
      return $response;
    }
    $response = new WechatXmlResponse($content);
    return $response;
  }

  public function rebuildCustomer() {
    $list_obj =$this->wechat->getUserList();
    foreach ($list_obj['data']['openid'] as $openid)
    {
      $this->wechat->customerUpdate($openid);
    }
  }

  public function callExtension(){
    $replyList = [];
    $extension = $this->wechat->getConfig()->get('extension')->value;
    $services = explode("\r\n",  $extension);
    foreach($services as $service) {
      if(\Drupal::hasService($service)) {
         $data = \Drupal::service($service)
          ->wechatExtra($this->wechat);
        if($data) {
          $replyList[] = $data;
        }
      }
    }
    if(count($replyList)>0) {
      return $this->wechat->toXML(implode('\r\n', $replyList));
    }else {
      return FALSE;
    }
  }
}
