<?php

/**
 * @file
 * Contains \Drupal\weixin\Controller\wxConfigController.
 */

namespace Drupal\one_weixin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\one_weixin\Entity\wxCustomer;
use Drupal\one_weixin\Entity\wxConfig;
use Drupal\one_weixin\impl\DrupalWechat;

/**
 * Class wxConfigController.
 *
 * @package Drupal\weixin\Controller
 */
class wxConfigController extends ControllerBase {
  /**
   * Menubuild.
   *
   * @return string
   *   Return Hello string.
   */
  public function menuBuild() {
    return [
        '#type' => 'markup',
        '#markup' => $this->t('Implement method: menuBuild')
    ];
  }
  /**
   * Messagelist.
   *
   * @return string
   *   Return Hello string.
   */
  public function messageList($wx_config) {
    $list_builder = $this->entityTypeManager()->getListBuilder('wx_message');
    $list_builder->setWid($wx_config);
    return $list_builder->render();
  }
  /**
   * Customerlist.
   *
   * @return string
   *   Return Hello string.
   */
  public function customerList($wx_config) {
    $wechat = new DrupalWechat($wx_config);
    $wechat->rebuildCustomer();
    $list_builder = $this->entityTypeManager()->getListBuilder('wx_customer');
    $list_builder->setWid($wx_config);
    return $list_builder->render();
  }
  /**
   * Replylist.
   *
   * @return string
   *   Return Hello string.
   */
  public function replyList($wx_config) {
    $list_builder = $this->entityTypeManager()->getListBuilder('wx_autoreply');
    $list_builder->setWid($wx_config);
    return $list_builder->render();
  }
}
