<?php

namespace Drupal\one_weixin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wx customer entities.
 *
 * @ingroup one_weixin
 */
interface wxCustomerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Wx customer name.
   *
   * @return string
   *   Name of the Wx customer.
   */
  public function getName();

  /**
   * Sets the Wx customer name.
   *
   * @param string $name
   *   The Wx customer name.
   *
   * @return \Drupal\one_weixin\Entity\wxCustomerInterface
   *   The called Wx customer entity.
   */
  public function setName($name);

  /**
   * Gets the Wx customer creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wx customer.
   */
  public function getCreatedTime();

  /**
   * Sets the Wx customer creation timestamp.
   *
   * @param int $timestamp
   *   The Wx customer creation timestamp.
   *
   * @return \Drupal\one_weixin\Entity\wxCustomerInterface
   *   The called Wx customer entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Wx customer published status indicator.
   *
   * Unpublished Wx customer are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Wx customer is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Wx customer.
   *
   * @param bool $published
   *   TRUE to set this Wx customer to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\Entity\wxCustomerInterface
   *   The called Wx customer entity.
   */
  public function setPublished($published);

}
