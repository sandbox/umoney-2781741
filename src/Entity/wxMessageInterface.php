<?php

namespace Drupal\one_weixin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wx message entities.
 *
 * @ingroup one_weixin
 */
interface wxMessageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Wx message name.
   *
   * @return string
   *   Name of the Wx message.
   */
  public function getName();

  /**
   * Sets the Wx message name.
   *
   * @param string $name
   *   The Wx message name.
   *
   * @return \Drupal\one_weixin\Entity\wxMessageInterface
   *   The called Wx message entity.
   */
  public function setName($name);

  /**
   * Gets the Wx message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wx message.
   */
  public function getCreatedTime();

  /**
   * Sets the Wx message creation timestamp.
   *
   * @param int $timestamp
   *   The Wx message creation timestamp.
   *
   * @return \Drupal\one_weixin\Entity\wxMessageInterface
   *   The called Wx message entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Wx message published status indicator.
   *
   * Unpublished Wx message are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Wx message is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Wx message.
   *
   * @param bool $published
   *   TRUE to set this Wx message to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\Entity\wxMessageInterface
   *   The called Wx message entity.
   */
  public function setPublished($published);

}
