<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Entity\wxConfig.
 */

namespace Drupal\one_weixin\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for wx config entities.
 */
class wxConfigViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['wx_config']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('wx config'),
      'help' => $this->t('The wx config ID.'),
    );

    return $data;
  }

}
