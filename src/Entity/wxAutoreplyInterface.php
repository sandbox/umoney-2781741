<?php

namespace Drupal\one_weixin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wx autoreply entities.
 *
 * @ingroup one_weixin
 */
interface wxAutoreplyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Wx autoreply name.
   *
   * @return string
   *   Name of the Wx autoreply.
   */
  public function getName();

  /**
   * Sets the Wx autoreply name.
   *
   * @param string $name
   *   The Wx autoreply name.
   *
   * @return \Drupal\one_weixin\Entity\wxAutoreplyInterface
   *   The called Wx autoreply entity.
   */
  public function setName($name);

  /**
   * Gets the Wx autoreply creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wx autoreply.
   */
  public function getCreatedTime();

  /**
   * Sets the Wx autoreply creation timestamp.
   *
   * @param int $timestamp
   *   The Wx autoreply creation timestamp.
   *
   * @return \Drupal\one_weixin\Entity\wxAutoreplyInterface
   *   The called Wx autoreply entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Wx autoreply published status indicator.
   *
   * Unpublished Wx autoreply are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Wx autoreply is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Wx autoreply.
   *
   * @param bool $published
   *   TRUE to set this Wx autoreply to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\Entity\wxAutoreplyInterface
   *   The called Wx autoreply entity.
   */
  public function setPublished($published);

}
