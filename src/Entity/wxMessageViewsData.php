<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Entity\wxMessage.
 */

namespace Drupal\one_weixin\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for wx message entities.
 */
class wxMessageViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['wx_message']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('wx message'),
      'help' => $this->t('The wx message ID.'),
    );

    return $data;
  }

}
