<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Entity\wxCustomer.
 */

namespace Drupal\one_weixin\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for wx customer entities.
 */
class wxCustomerViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['wx_customer']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('wx customer'),
      'help' => $this->t('The wx customer ID.'),
    );

    return $data;
  }

}
