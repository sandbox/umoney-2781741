<?php

namespace Drupal\one_weixin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Wx config entities.
 *
 * @ingroup one_weixin
 */
interface wxConfigInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Wx config name.
   *
   * @return string
   *   Name of the Wx config.
   */
  public function getName();

  /**
   * Sets the Wx config name.
   *
   * @param string $name
   *   The Wx config name.
   *
   * @return \Drupal\one_weixin\Entity\wxConfigInterface
   *   The called Wx config entity.
   */
  public function setName($name);

  /**
   * Gets the Wx config creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Wx config.
   */
  public function getCreatedTime();

  /**
   * Sets the Wx config creation timestamp.
   *
   * @param int $timestamp
   *   The Wx config creation timestamp.
   *
   * @return \Drupal\one_weixin\Entity\wxConfigInterface
   *   The called Wx config entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Wx config published status indicator.
   *
   * Unpublished Wx config are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Wx config is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Wx config.
   *
   * @param bool $published
   *   TRUE to set this Wx config to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\Entity\wxConfigInterface
   *   The called Wx config entity.
   */
  public function setPublished($published);

}
