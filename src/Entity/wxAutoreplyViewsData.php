<?php

/**
 * @file
 * Contains \Drupal\one_weixin\Entity\wxAutoreply.
 */

namespace Drupal\one_weixin\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for wx autoreply entities.
 */
class wxAutoreplyViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['wx_autoreply']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('wx autoreply'),
      'help' => $this->t('The wx autoreply ID.'),
    );

    return $data;
  }

}
