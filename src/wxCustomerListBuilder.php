<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxCustomerListBuilder.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of wx customer entities.
 *
 * @ingroup weixin
 */
class wxCustomerListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  protected $wid;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('wx customer ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\one_weixin\Entity\wxCustomer */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.wx_customer.edit_form', array(
          'wx_customer' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }
  public function setWid($wid) {
    $this->wid = $wid;
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = \Drupal::entityQuery('wx_customer');
    if(isset($this->wid)) {
      $nids = $query
        ->condition('wid', $this->wid)
        ->sort('created', 'DESC')
        ->execute();
      return $nids;
    }else {
      $nids = $query
        ->sort('created', 'DESC')
        ->execute();
      return $nids;
    }
  }
}
