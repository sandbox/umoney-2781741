<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxConfigAccessControlHandler.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the wx config entity.
 *
 * @see \Drupal\one_weixin\Entity\wxConfig.
 */
class wxConfigAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\one_weixin\wxConfigInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished wx config entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published wx config entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit wx config entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete wx config entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add wx config entities');
  }

}
