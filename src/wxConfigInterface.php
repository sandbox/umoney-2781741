<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxConfigInterface.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining wx config entities.
 *
 * @ingroup weixin
 */
interface wxConfigInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the wx config name.
   *
   * @return string
   *   Name of the wx config.
   */
  public function getName();

  /**
   * Sets the wx config name.
   *
   * @param string $name
   *   The wx config name.
   *
   * @return \Drupal\one_weixin\wxConfigInterface
   *   The called wx config entity.
   */
  public function setName($name);

  /**
   * Gets the wx config creation timestamp.
   *
   * @return int
   *   Creation timestamp of the wx config.
   */
  public function getCreatedTime();

  /**
   * Sets the wx config creation timestamp.
   *
   * @param int $timestamp
   *   The wx config creation timestamp.
   *
   * @return \Drupal\one_weixin\wxConfigInterface
   *   The called wx config entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the wx config published status indicator.
   *
   * Unpublished wx config are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the wx config is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a wx config.
   *
   * @param bool $published
   *   TRUE to set this wx config to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\wxConfigInterface
   *   The called wx config entity.
   */
  public function setPublished($published);

}
