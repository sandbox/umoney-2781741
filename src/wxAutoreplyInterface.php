<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxAutoreplyInterface.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining wx autoreply entities.
 *
 * @ingroup weixin
 */
interface wxAutoreplyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the wx autoreply name.
   *
   * @return string
   *   Name of the wx autoreply.
   */
  public function getName();

  /**
   * Sets the wx autoreply name.
   *
   * @param string $name
   *   The wx autoreply name.
   *
   * @return \Drupal\one_weixin\wxAutoreplyInterface
   *   The called wx autoreply entity.
   */
  public function setName($name);

  /**
   * Gets the wx autoreply creation timestamp.
   *
   * @return int
   *   Creation timestamp of the wx autoreply.
   */
  public function getCreatedTime();

  /**
   * Sets the wx autoreply creation timestamp.
   *
   * @param int $timestamp
   *   The wx autoreply creation timestamp.
   *
   * @return \Drupal\one_weixin\wxAutoreplyInterface
   *   The called wx autoreply entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the wx autoreply published status indicator.
   *
   * Unpublished wx autoreply are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the wx autoreply is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a wx autoreply.
   *
   * @param bool $published
   *   TRUE to set this wx autoreply to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\wxAutoreplyInterface
   *   The called wx autoreply entity.
   */
  public function setPublished($published);

}
