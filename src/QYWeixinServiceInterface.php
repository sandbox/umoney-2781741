<?php

namespace Drupal\one_weixin;

/**
 * Interface QYWeixinServiceInterface.
 *
 * @package Drupal\weixin
 */
interface QYWeixinServiceInterface {

  public function sendMesg($wid,$user,$party,$tag,$text) ;

}
