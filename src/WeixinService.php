<?php

namespace Drupal\one_weixin;
use Drupal\one_weixin\impl\DrupalWechat;

/**
 * Class WeixinService.
 *
 * @package Drupal\weixin
 */
class WeixinService implements WeixinServiceInterface {

  /**
   * Constructor.
   */
  public function __construct() {

  }
  public  function getQRCode($wid,$scene_id,$type=0,$expire=604800){
    $wechat = new DrupalWechat($wid);
    return $wechat->getQRCode($scene_id,$type,$expire);
  }

  public  function sendTemplateMassage($wid,$open_id,$template_id, $url,$data,$remark=''){
    $wechat = new DrupalWechat($wid);
    return $wechat->sendTemplateMassage($open_id,$template_id, $url,$data,$remark);
  }

  public  function getUserInfos($wid,$oids){
    $wechat = new DrupalWechat($wid);
    return $wechat->getUserInfos($oids);
  }
  public function getUserInfo($wid,$openid){
    $wechat = new DrupalWechat($wid);
    return $wechat->getUserInfo($openid);

  }

}
