<?php

namespace Drupal\one_weixin;
use Drupal\one_weixin\impl\DrupalQYWechat;


/**
 * Class QYWeixinService.
 *
 * @package Drupal\weixin
 */
class QYWeixinService implements QYWeixinServiceInterface {

  /**
   * Constructor.
   */
  public function __construct() {

  }
  
  public function sendMesg($wid,$user,$party,$tag,$text) {
    $wechat = new DrupalQYWechat($wid);
    return $wechat->sendMesg($user,$party,$tag,$text);
  }
}
