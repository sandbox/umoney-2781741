<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxMessageInterface.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining wx message entities.
 *
 * @ingroup weixin
 */
interface wxMessageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the wx message name.
   *
   * @return string
   *   Name of the wx message.
   */
  public function getName();

  /**
   * Sets the wx message name.
   *
   * @param string $name
   *   The wx message name.
   *
   * @return \Drupal\one_weixin\wxMessageInterface
   *   The called wx message entity.
   */
  public function setName($name);

  /**
   * Gets the wx message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the wx message.
   */
  public function getCreatedTime();

  /**
   * Sets the wx message creation timestamp.
   *
   * @param int $timestamp
   *   The wx message creation timestamp.
   *
   * @return \Drupal\one_weixin\wxMessageInterface
   *   The called wx message entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the wx message published status indicator.
   *
   * Unpublished wx message are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the wx message is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a wx message.
   *
   * @param bool $published
   *   TRUE to set this wx message to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\one_weixin\wxMessageInterface
   *   The called wx message entity.
   */
  public function setPublished($published);

}
