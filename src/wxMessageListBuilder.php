<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxMessageListBuilder.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of wx message entities.
 *
 * @ingroup weixin
 */
class wxMessageListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  protected $wid;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('wx message ID');
    $header['title'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\one_weixin\Entity\wxMessage */
    $row['id'] = $entity->id();
    $row['title'] = $this->l(
      $entity->label(),
      new Url(
        'entity.wx_message.edit_form', array(
          'wx_message' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }
  public function setWid($wid) {
    $this->wid = $wid;
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = \Drupal::entityQuery('wx_message');
    $nids = $query
      ->condition('wid', $this->wid)
      ->sort('created', 'DESC')
      ->execute();
    return $nids;
  }
}
