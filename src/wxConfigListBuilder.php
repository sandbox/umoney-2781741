<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxConfigListBuilder.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of wx config entities.
 *
 * @ingroup weixin
 */
class wxConfigListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('wx config ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\one_weixin\Entity\wxConfig */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.wx_config.edit_form', array(
          'wx_config' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }
  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

//    if ($entity->access('update') && $entity->hasLinkTemplate('menu-form')) {
      $operations['wxmenu'] = array(
        'title' => $this->t('Menu'),
        'weight' => 20,
        'url' => $entity->toUrl('menu-form'),
      );
    $operations['wxmessage'] = [
      'title' => $this->t('Message'),
      'weight' => 22,
      'url' => Url::fromRoute('weixin.wx_config_controller_messageList', [
        'wx_config' => $entity->id()
      ]),
    ];

    $operations['wxcustomer'] = [
      'title' => $this->t('Customer'),
      'weight' => 23,
      'url' => Url::fromRoute('weixin.wx_config_controller_customerList', [
        'wx_config' => $entity->id()
      ]),
    ];

    $operations['wxreply'] = [
      'title' => $this->t('Auto Reply'),
      'weight' => 24,
      'url' => Url::fromRoute('weixin.wx_config_controller_replyList', [
        'wx_config' => $entity->id()
      ]),
    ];
//    }

    return $operations;
  }

}
