<?php
/**
 * Created by PhpStorm.
 * User: horizon
 * Date: 2016/8/5
 * Time: 6:17
 */

namespace Drupal\one_weixin\impl;

require_once dirname(__DIR__) . '/../lib/qywechat.class.php';

use Drupal\Core\Controller\ControllerBase;
use Drupal\one_weixin\Entity\wxAutoreply;
use Drupal\one_weixin\Entity\wxCustomer;
use Drupal\one_weixin\Entity\wxMessage;
use qyPrpcrypt;
use qyWechat;
use Drupal\one_weixin\Entity\wxConfig;
use Drupal\one_weixin\impl\wxCommon;

class DrupalQYWechat {
  protected $wechat;
  protected $wid;
  protected $config;
  public function __construct($wid,$options=NULL,$content=NULL)
  {
    if($wid)
    {
      $this->wid = $wid;
      $config = wxConfig::Load($wid);
      $options = array(
        'token'=>$config->getToken(), //填写你设定的key
        'encodingaeskey'=>$config->getAESKey(), //填写加密用的EncodingAESKey
        'appid'=>$config->getAppid(), //填写高级调用功能的app id
        'appsecret'=>$config->getAppSecret() //填写高级调用功能的密钥
      );
      $this->config = $config;
      $this->wechat = new qyWechat($options);
      if($content) {
        $this->wechat->getRev($content);
      }
    }
  }
  public function getId() {
    return $this->wid;
  }

  public function getConfig() {
    return $this->config;
  }
  public function getMsgType() {
    return strtolower($this->wechat->getRevType());
  }
  public function getMsgId() {
    return $this->wechat->getRevID();
  }
  public function getEvent() {
    return  $this->wechat->getRevEvent();
  }

  public  function getQRCode($scene_id,$type=0,$expire=604800){
    return $this->wechat->getQRCode($scene_id,$type,$expire);
  }

  public function getFromUser() {
    return $this->wechat->getRevFrom();
  }
  public function getMenu() {
    return $this->wechat->getMenu();
  }
  public function createMenu($data) {
    return $this->wechat->createMenu($data);
  }
  public function getUserInfo($openid){
    $user = $this->wechat->getUserInfo($openid);
    if($user) {
      $user['nickname'] = wxCommon::removeEmoji($user['nickname']);
      $user['wid'] = $this->wid;
    }
    return $user;
  }

  public function getUserInfos($oids){
    $users = [];
    foreach($oids as $oid) {
      $user = $this->wechat->getUserInfo($oid);
      if($user) {
        $user['nickname'] = wxCommon::removeEmoji($user['nickname']);
        $user['wid'] = $this->wid;
        $users[] = $user;
      }
    }
    return $users;
  }

  public function sendTemplateMassage($open_id,$template_id, $url,$data,$remark=''){

    $msg = array('touser'=>$open_id,
      'template_id'=>$template_id,
      'url'=>$url,
      'data'=>$data,
    );
    $this->wechat->sendTemplateMessage($msg);
  }
  
  public function checkSignature($params,$str=''){
    return $this->wechat->checkSignature($params,$str);
  }

  public function decrypt($encryptStr){
    $pc = new qyPrpcrypt($this->config->getAESKey());
    $array = $pc->decrypt($encryptStr,$this->config->getAppid());
    \Drupal::logger('qyweixin')->notice('array @aa,@bb',array('@aa'=>var_export($array,TRUE),'@bb'=>var_export($encryptStr,TRUE)));
    $echoStr = $array[1];
    return $echoStr;
//    return $this->wechat->checkSignature($params,$str);
  }
  public function getReplyMsg($default = 'hello world'){
    $welcome = $default;
    $type = $this->getMsgType();
    switch(strtolower($type)) {
      case wxCommon::MSGTYPE_TEXT:
        $content = $this->wechat->getRevContent();
        $query = \Drupal::entityQuery('wx_autoreply');
        $nids = $query
          ->condition('wid', $this->wid)
          ->execute();
        foreach ($nids as $nid) {
          $reply = wxAutoreply::Load($nid);
          if (strstr($content, $reply->get('keyword')->value)) {
            $welcome = $reply->get('reply_text')->value;
            break;
          }
        }
        break;
      case wxCommon::MSGTYPE_EVENT:
        $event = $this->wechat->getRevEvent();
        if($event['event']===wxCommon::EVENT_SCAN) {
          $welcome = $this->config->get('scene_welcome')->value;
        }
        else {
          $welcome = $this->config->get('welcome')->value;
        }
        break;
      case wxCommon::MSGTYPE_IMAGE:
        break;
      default:
        break;
    }
    $xmlstr = $this->wechat->text($welcome)->reply(array(), TRUE);
    return $xmlstr;
  }
  public  function msgUpdate() {
    $msgid = $this->wechat->getRevID();
    if (isset($msgid)) {
      $query = \Drupal::entityQuery('wx_message');
      $nids = $query
        ->condition('msg_id', $msgid)
        ->execute();
      if (count($nids) == 0) {
        $msgType = $this->wechat->getRevType();
        $msgType = strtolower($msgType);
        $body = Array(
          'open_id' => $this->wechat->getRevFrom(),
          'mxtype' => $msgType,
          'wid' => $this->wid,
          'msg_id' => $msgid,
          'to_user' => $this->wechat->getRevTo(),
          'from_user_name' => $this->wechat->getRevFrom(),
        );
        if ($msgType === wxCommon::MSGTYPE_TEXT) {
          $body['content'] = $this->wechat->getRevContent();
          $body['title'] = $this->wechat->getRevContent();
        }
        $msg = wxMessage::create($body);
        $msg->save();

      }
    }
  }

  public  function eventUpdate() {

    $msgType = $this->wechat->getRevType();
    $msgType = strtolower($msgType);
    $body = Array(
      'open_id' => $this->wechat->getRevFrom(),
      'mxtype' => $msgType,
      'wid' => $this->wid,
      'msg_id' => 'event',
      'to_user' => $this->wechat->getRevTo(),
      'from_user_name' => $this->wechat->getRevFrom(),
      'changed'=>$this->wechat->getRevCtime(),
    );
    $event = $this->wechat->getRevEvent();
    $body['event'] = $event['event'];
    if(isset($event['key'])) {
      $body['event_key'] = $event['key'];
      if(strpos($event['key'],'qrscene_')===0){
        $body['event_key'] = substr($event['key'],8);
      }
    }
    $msg = wxMessage::create($body);
    $msg->save();
  }

  public function customerUpdate($openid) {
    $query = \Drupal::entityQuery('wx_customer');
    $nids = $query
      ->condition('openid', $openid)
      ->execute();
    if (count($nids) == 0) {
      $body = $this->getUserInfo($openid);
//      \Drupal::logger('weixin')->notice('call openid  @aa,@bb ',array('@aa'=>$openid,'@bb'=>var_export($body,TRUE)));

      $body['nickname'] = wxCommon::removeEmoji($body['nickname']);
      $body['wid'] = $this->wid;

      $customer = wxCustomer::create($body);
      $customer->save();
    }
  }
  public function getUserMsg(){
    return $this->wechat->getRevContent();
  }
  public function toXML($text){
    $xmlstr = $this->wechat->text($text)->reply(array(), TRUE);
    return $xmlstr;
  }

  public function rebuildCustomer() {
    $list_obj =$this->wechat->getUserList();
    foreach ($list_obj['data']['openid'] as $openid)
    {
      $this->customerUpdate($openid);
    }
  }
public function sendMesg($user,$party,$tag,$text){
  $data=array();
  if(isset($user)){$data['touser']=$user;};
  if(isset($party)){$data['toparty']=$party;};
  if(isset($user)){$data['totag']=$tag;};
  $data['text']=array('content'=>$text);
  $data['msgtype']='text';
  $data['agentid']=001;
  $this->wechat->sendMessage($data);
}
}