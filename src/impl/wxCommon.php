<?php

namespace Drupal\one_weixin\impl;

class wxCommon {
  const MSGTYPE_TEXT = 'text';
  const MSGTYPE_IMAGE = 'image';
  const MSGTYPE_LOCATION = 'location';
  const MSGTYPE_LINK = 'link';
  const MSGTYPE_EVENT = 'event';
  const MSGTYPE_MUSIC = 'music';
  const MSGTYPE_NEWS = 'news';
  const MSGTYPE_VOICE = 'voice';
  const MSGTYPE_VIDEO = 'video';
  const EVENT_SUBSCRIBE = 'subscribe';       //订阅
  const EVENT_UNSUBSCRIBE = 'unsubscribe';   //取消订阅
  const EVENT_SCAN = 'SCAN';                 //扫描带参数二维码
  const EVENT_LOCATION = 'LOCATION';         //上报地理位置
  const EVENT_MENU_VIEW = 'VIEW';                     //菜单 - 点击菜单跳转链接
  const EVENT_MENU_CLICK = 'CLICK';                   //菜单 - 点击菜单拉取消息
  const EVENT_MENU_SCAN_PUSH = 'scancode_push';       //菜单 - 扫码推事件(客户端跳URL)
  const EVENT_MENU_SCAN_WAITMSG = 'scancode_waitmsg'; //菜单 - 扫码推事件(客户端不跳URL)
  const EVENT_MENU_PIC_SYS = 'pic_sysphoto';          //菜单 - 弹出系统拍照发图
  const EVENT_MENU_PIC_PHOTO = 'pic_photo_or_album';  //菜单 - 弹出拍照或者相册发图
  const EVENT_MENU_PIC_WEIXIN = 'pic_weixin';         //菜单 - 弹出微信相册发图器
  const EVENT_MENU_LOCATION = 'location_select';      //菜单 - 弹出地理位置选择器
//处理微信中的emoji表情符号
  public static function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
  }

}