<?php

namespace Drupal\one_weixin;

/**
 * Interface WeixinServiceInterface.
 *
 * @package Drupal\weixin
 */
interface WeixinServiceInterface {

  public  function getQRCode($wid,$scene_id,$type=0,$expire=604800);

  public  function sendTemplateMassage($wid,$open_id,$template_id, $url,$data,$remark='');

  public  function getUserInfos($wid,$oids);
  public function getUserInfo($wid,$openid);

}
