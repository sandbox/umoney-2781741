<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxCustomerAccessControlHandler.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the wx customer entity.
 *
 * @see \Drupal\one_weixin\Entity\wxCustomer.
 */
class wxCustomerAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\one_weixin\wxCustomerInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished wx customer entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published wx customer entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit wx customer entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete wx customer entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add wx customer entities');
  }

}
