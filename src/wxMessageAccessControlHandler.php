<?php

/**
 * @file
 * Contains \Drupal\one_weixin\wxMessageAccessControlHandler.
 */

namespace Drupal\one_weixin;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the wx message entity.
 *
 * @see \Drupal\one_weixin\Entity\wxMessage.
 */
class wxMessageAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\one_weixin\wxMessageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished wx message entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published wx message entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit wx message entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete wx message entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add wx message entities');
  }

}
