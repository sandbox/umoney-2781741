<?php

/**
 * @file
 * Contains wx_customer.page.inc..
 *
 * Page callback for Wx customer entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Wx customer templates.
 *
 * Default template: wx_customer.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_wx_customer(array &$variables) {
  // Fetch WxCustomer Entity Object.
  $wx_customer = $variables['elements']['#wx_customer'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
